import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.http.*;
public class HelloWorld extends HttpServlet {

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp)
            throws ServletException, IOException {
        resp.getWriter().print("Hello from Sarah!\n");
        resp.getWriter().print("Imported project into Eclipse and am working with it there.\n");
    }
}
