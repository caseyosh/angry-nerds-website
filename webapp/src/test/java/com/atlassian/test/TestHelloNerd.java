package com.atlassian.test;

import com.gargoylesoftware.htmlunit.BrowserVersion;
import com.gargoylesoftware.htmlunit.Page;
import com.gargoylesoftware.htmlunit.WebClient;
import com.gargoylesoftware.htmlunit.html.HtmlElement;
import com.gargoylesoftware.htmlunit.html.HtmlPage;
import com.gargoylesoftware.htmlunit.html.HtmlParagraph;
import org.apache.commons.lang.StringUtils;
import org.testng.Assert;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

public class TestHelloNerd {

    HtmlPage page;

    @BeforeTest
    public void setup() throws Exception
    {
        //Get the system property passed via maven
        String angrynerdsUrl = System.getProperty("angrynerds.url");

        //If the test is run through the IDE and the webapp is being run with mvn jetty:run, then angrynerds.url hasn't been set
        if (StringUtils.isEmpty(angrynerdsUrl))
        {
            angrynerdsUrl = "http://localhost:8080/angrynerds/";
        }

        WebClient webClient = new WebClient(BrowserVersion.getDefault());

        //html unit does not like the javascript in our page
        webClient.setJavaScriptEnabled(false);

        page = webClient.getPage(angrynerdsUrl);
    }
	
	@Test (groups = {"batch1", "integration-tests"})
	public void testHasIntroText() throws Exception {


        HtmlParagraph intro = page.getHtmlElementById("intro");

        Assert.assertEquals("What do you do when you have bugs in your code? Send in the Angry Nerds. Consider these issues…resolved.",
                StringUtils.trim(intro.getTextContent()));
	}
	
	@Test (groups = {"batch2", "integration-tests"})
	public void testHasMerchHeading() throws Exception {

        HtmlParagraph intro = page.getHtmlElementById("intro");

        Assert.assertEquals("What do you do when you have bugs in your code? Send in the Angry Nerds. Consider these issues…resolved.",
                StringUtils.trim(intro.getTextContent()));
	}
	
	@Test (groups = {"batch2", "integration-tests"})
	public void testPlaysBoardGames() throws Exception {

        HtmlParagraph intro = page.getHtmlElementById("intro");

        Assert.assertEquals("What do you do when you have bugs in your code? Send in the Angry Nerds. Consider these issues…resolved.",
                StringUtils.trim(intro.getTextContent()));
	}
	
	@Test (groups = {"batch1", "integration-tests"})
	public void testBelongsToChessClub() throws Exception {

        HtmlParagraph intro = page.getHtmlElementById("intro");

        Assert.assertEquals("What do you do when you have bugs in your code? Send in the Angry Nerds. Consider these issues…resolved.",
                StringUtils.trim(intro.getTextContent()));
	}
}
